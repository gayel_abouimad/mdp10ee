
class Commands :
    def __init__(self, ResistorisOn = False, Speed = 0 , horizontalRotation = 0, verticalRotation = 0):
        self.ResistorisOn = ResistorisOn
        self.Speed = Speed
        self.horizontalRotation = horizontalRotation
        self.verticalRotation = verticalRotation

    def setRes(self, state):
        self.ResistorisOn = state

    def setSpeed(self, value):
        self.Speed=value

    def setHorz(self, value):
        self.horizontalRotation=value

    def setVert(self, value):
        self.verticalRotation=value

    def getRes(self):
        return self.ResistorisOn

    def getSpeed(self):
        return self.Speed

    def getHorz(self):
        return self.horizontalRotation

    def getVert(self):
        return self.verticalRotation

# added hello world
