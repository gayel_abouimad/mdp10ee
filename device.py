from umqtt import MQTTClient
from network import WLAN
import machine
import time
import json
from commands import Commands

cmds = Commands()
def sub_cb(topic, msg):
    if msg.startswith("{") and msg.endswith("}"):
        print("Recieved JSON")
        result = json.loads(msg)
    else:
        print("Recieved Wrong String")
        print(msg.upper())
    generateCommands(result)

def generateCommands(res):
    i = 0
    keys = list(res.keys())
    while (i < len(keys)):
        if(keys[i] == 'speed'):
            cmds.setSpeed(res['speed'])
        if(keys[i] == 'ResistorisOn'):
            cmds.setRes(res['ResistorisOn'])
        if(keys[i] == 'horizontalRotation'):
            cmds.setHorz(res['horizontalRotation'])
        if(keys[i] == 'verticalRotation'):
            cmds.setVert(res['verticalRotation'])
        i+=1
    print("res", cmds.getRes())
    print("speed", cmds.getSpeed())
    print("horrot", cmds.getHorz())
    print("vertrot", cmds.getVert())
    print("\n")

class DeviceConnection :
    def __init__(self, brokerAddress = "212.98.137.194", deviceID = "MDP10EE", username = "iotleb", password = "iotleb", port = 1883, topic = "nicolas"):
        self.brokerAddress = brokerAddress
        self.deviceID = deviceID
        self.username = username
        self.password = password
        self.port = port
        self.topic = topic

    def connectToWifi(self, wifiname = "Gayo", wifipassword = "Gayel123"):
        wlan = WLAN(mode=WLAN.STA)
        wlan.connect(wifiname, auth=(WLAN.WPA2, wifipassword), timeout=5000)
        while not wlan.isconnected():
            machine.idle()
        print("Connected to WiFi\n")

    def connectToServer(self):
        client = MQTTClient(self.deviceID, self.brokerAddress, user=self.username, password = self.password, port = self.port)
        client.set_callback(sub_cb)
        client.connect()
        client.subscribe(self.topic)
        while True:
            client.wait_msg()
            time.sleep(1)


#wlan.connect("Nicolas's iPhone", auth=(WLAN.WPA2, "nicolas1"), timeout=5000)
#client = MQTTClient("device_id", "io.adafruit.com",user="NicolasAttieh", password="ab6aa4a497294cfcab915f95b077f900", port=1883)
